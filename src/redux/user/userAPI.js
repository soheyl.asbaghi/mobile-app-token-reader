import axios from 'axios';
import store from '../store';
import {
  fetchUsersRequest,
  fetchUsersSuccess,
  fetchUsersFailure,
} from './userActions';

export const fetchUser = () => {
  return dispatch => {
    const token = store.getState().user.token;
    dispatch(fetchUsersRequest());
    axios
      .get('/user', {headers: {Authorization: `Bearer ${token}`}})
      .then(res => {
        const user = res.data;
        dispatch(fetchUsersSuccess(user));
      })
      .catch(err => {
        const error = err.message;
        dispatch(fetchUsersFailure(error));
      });
  };
};
