import {StyleSheet} from 'react-native';

export const globalSyles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 40,
  },
});
