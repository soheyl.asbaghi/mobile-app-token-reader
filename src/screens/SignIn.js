import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
  Image,
  SafeAreaView,
  ScrollView,
  StatusBar,
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {connect} from 'react-redux';
import {fetchUser, fetchUserToken} from '../redux';

const SignIn = ({navigation, userData, tokenChange, onFetchUser}) => {
  // fcBiuNs7sWzHsNzjcfr3
  console.log(userData);
  const visibilityHandler = () => {
    onFetchUser();
  };

  return (
    // <TouchableWithoutFeedback onPress={() => Keyboard.addListener()}>
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <View>
          <Image
            source={require('../assets/Flying_kite_Outline.png')}
            style={styles.img}
          />
          <Text style={styles.title}>Enter your Token</Text>
          <TextInput
            style={styles.input}
            placeholder="Token..."
            onChangeText={e => tokenChange(e)}
          />
          <TouchableOpacity style={styles.btn} onPress={visibilityHandler}>
            <Icon name="send" size={15} color="#fff" />
          </TouchableOpacity>
        </View>
        <View>
          {userData.loading ? (
            <ActivityIndicator size="large" color="#292961" />
          ) : userData.error ? (
            <Text style={styles.error}>OOPS! {userData.error}</Text>
          ) : userData.error === '' || userData.error ? (
            <TouchableOpacity
              onPress={() => navigation.push('UserInfo')}
              style={styles.navBtn}>
              <Text>Show Profile Info</Text>
            </TouchableOpacity>
          ) : null}
        </View>
      </ScrollView>
    </SafeAreaView>
    // </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flex: 1,
    paddingTop: StatusBar.currentHeight,
    alignItems: 'center',
  },
  title: {
    marginBottom: 20,
    fontSize: 25,
    fontWeight: 'bold',
    padding: 5,
  },
  input: {
    height: 60,
    padding: 20,
    marginBottom: 20,
    borderWidth: 0.5,
    borderColor: '#292961',
    borderRadius: 10,
  },
  btn: {
    backgroundColor: '#292961',
    padding: 10,
    position: 'relative',
    top: -68,
    left: 120,
    borderRadius: 10,
    alignSelf: 'center',
  },
  navBtn: {
    backgroundColor: '#80ED99',
    alignSelf: 'center',
    padding: 12,
  },
  error: {
    fontWeight: 'bold',
    color: 'tomato',
    textAlign: 'center',
  },
  img: {
    width: 300,
    height: 300,
  },
});

const mapStateToProps = state => {
  return {
    userData: state.user,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchUser: () => dispatch(fetchUser()),
    tokenChange: e => dispatch(fetchUserToken(e)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
