import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import {connect} from 'react-redux';
import {fetchUser, fetchUserToken} from '../redux';

const UserInfo = ({navigation, userData, tokenChange, onFetchUser}) => {
  // fcBiuNs7sWzHsNzjcfr3

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <View>
          <View style={styles.content}>
            <TouchableOpacity>
              <Image
                source={{uri: `${userData.users.avatar_url}`}}
                style={styles.avatar}
              />
            </TouchableOpacity>
            <Text style={styles.name}>{userData.users.name}</Text>
            <Text style={styles.follow}>
              Followers {userData.users.followers}
            </Text>
            <Text style={styles.follow}>
              Following {userData.users.following}
            </Text>
            <View>
              <Text style={styles.userName}>{userData.users.username}</Text>
              <Text style={styles.email}>{userData.users.email}</Text>
            </View>

            <TouchableOpacity onPress={() => navigation.push('ProjectList')}>
              <Text>click me</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 9,
    height: '100%',
  },
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    width: 50,
    height: 50,
    borderRadius: 100,
    alignSelf: 'flex-end',
    marginLeft: 2,
  },
});

const mapStateToProps = state => {
  return {
    userData: state.user,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchUser: () => dispatch(fetchUser()),
    tokenChange: e => dispatch(fetchUserToken(e)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserInfo);
