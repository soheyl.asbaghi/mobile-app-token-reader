import React from 'react';
import {Provider} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import axios from 'axios';

import SignIn from './src/screens/SignIn';
import ProjectList from './src/screens/ProjectList';
import store from './src/redux/store';
import UserInfo from './src/screens/UserInfo';

axios.defaults.baseURL = 'https://gitlab.com/api/v4';

const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Home"
            component={SignIn}
            options={{title: 'Login'}}
          />
          <Stack.Screen
            name="UserInfo"
            component={UserInfo}
            options={{title: 'Profile'}}
          />
          <Stack.Screen
            name="ProjectList"
            component={ProjectList}
            options={{title: 'Projects'}}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

export default App;
